namespace Grupo7_Market.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdicionTablaFactura : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Factura", "OrdenId", c => c.Int(nullable: false));
            AddColumn("dbo.Factura", "DetalleId", c => c.Int(nullable: false));
            CreateIndex("dbo.Factura", "OrdenId");
            CreateIndex("dbo.Factura", "DetalleId");
            AddForeignKey("dbo.Factura", "OrdenId", "dbo.Orden", "OrdenId");
            AddForeignKey("dbo.Factura", "DetalleId", "dbo.OrdenDetalle", "DetalleId");
            DropColumn("dbo.Factura", "OrdenDetalleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Factura", "OrdenDetalleId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Factura", "DetalleId", "dbo.OrdenDetalle");
            DropForeignKey("dbo.Factura", "OrdenId", "dbo.Orden");
            DropIndex("dbo.Factura", new[] { "DetalleId" });
            DropIndex("dbo.Factura", new[] { "OrdenId" });
            DropColumn("dbo.Factura", "DetalleId");
            DropColumn("dbo.Factura", "OrdenId");
        }
    }
}
