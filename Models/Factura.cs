﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Grupo7_Market.Models
{
    public class Factura
    {


        public int FacturaId { get; set; }

        public string Cantidad { get; set; }

        public int ClienteId { get; set; }
        public Clientes Clientes { get; set; }

        public int OrdenId { get; set; }
        public Orden Orden { get; set; }

        public int  DetalleId { get; set; }
        public OrdenDetalle OrdenDetalle {get; set;}

        public int Subtotal { get; set; }

         [Required]
         public int ISV { get; set; }

        public int Total { get; set; }

        }
    }

